;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid hash-table-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid comparator)
	  (rapid hash-table))
  (begin

    (define default-comparator (make-default-comparator))

    (define ht (hash-table default-comparator 'a 1 'b 2 'c 3 'd 4))

    (define ht1
      (let ((ht (make-hash-table default-comparator)))
	(do ((i 0 (+ i 1)))
	    ((= i 500))
	  (hash-table-set! ht i (* i i)))
	(do ((i 0 (+ i 2)))
	    ((= i 500))
	  (hash-table-delete! ht i))
	(do ((i 500 (+ i 1)))
	    ((= i 1000))
	  (hash-table-set! ht i (* i i)))
	(do ((i 500 (+ i 2)))
	    ((= i 1000))
	  (hash-table-delete! ht i))
	ht))

    (define (run-tests)
      (test-begin "Hash tables")

      (test-assert "hash-table?: #t"
	(hash-table? (make-hash-table default-comparator)))

      (test-assert "hash-table?: #f"
	(not (hash-table? (vector #f))))

      (test-assert "hash-table-contains?: #t"
        (hash-table-contains? ht 'a))

      (test-assert "hash-table-contains?: #f"
        (not (hash-table-contains? ht 'e)))

      (test-assert "hash-table-contains?"
	(let loop ((i 0))
	  (cond
	   ((= i 1000)
	    #t)
	   ((odd? i)
	    (and (hash-table-contains? ht1 i)
		 (loop (+ i 1))))
	   ((even? i)
	    (and (not (hash-table-contains? ht1 i))
		 (loop (+ i 1)))))))

      (test-assert "hash-table=?"
        (hash-table=? default-comparator ht (hash-table-copy ht)))

      (test-assert "hash-table-empty?: #t"
	(hash-table-empty? (make-hash-table default-comparator)))

      (test-assert "hash-table-empty?: #f"
	(not (hash-table-empty? ht1)))

      (test-assert "hash-table-mutable?"
	(boolean? (hash-table-mutable? (hash-table default-comparator))))

      ;; Accessors

      (test-eqv "hash-table-ref: I"
	2
	(hash-table-ref ht 'b))

      (test-eqv "hash-table-ref: II"
	5
	(hash-table-ref ht 'e (lambda () 5)))

      (test-eqv "hash-table-ref: III"
	9
	(hash-table-ref ht 'c (lambda () 0) (lambda (i) (* i i))))

      (test-assert "hash-table-ref: IV"
	(let loop ((i 1))
	  (cond
	   ((>= i 1000)
	    #t)
	   (else
	    (and (= (hash-table-ref ht1 i) (* i i))
		 (loop (+ i 2)))))))

      (test-eqv "hash-table-ref/default"
	5
	(hash-table-ref/default ht 'e 5))

      ;; The whole hash table

      (test-eqv "Hash table size I"
	4
	(hash-table-size ht))

      (test-eqv "Hash table size II"
	500
	(hash-table-size ht1))

      (test-eqv "hash-table-count"
	167
	(hash-table-count (lambda (key value)
			    (zero? (modulo key 3)))
			  ht1))

      (test-end))))
