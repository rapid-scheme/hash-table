;;; Rapid Scheme --- An implementation of R7RS

;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; Types

(define-record-type <hash-table>
  (%make-hash-table comparator buckets size head)
  hash-table?
  (comparator hash-table-comparator)
  (buckets hash-table-buckets hash-table-set-buckets!)
  (size hash-table-size hash-table-set-size!)
  (head hash-table-head hash-table-set-head!))

;; Constructors

(define (make-hash-table comparator)
  (%make-hash-table comparator (make-vector 256 #f) 0 #f))

(define (hash-table comparator . args)
  (let ((hash-table (make-hash-table comparator)))
    (apply hash-table-set! hash-table args)
    hash-table))

(define (hash-table-unfold stop? mapper successor seed comparator)
  (let ((hash-table (make-hash-table comparator)))
    (do ((seed seed (successor seed)))
	((stop? seed) hash-table)
      (receive (key value) (mapper seed)
        (hash-table-set! hash-table key value)))))

(define (alist->hash-table alist comparator)
  (hash-table-unfold null?
		     (lambda (alist)
		       (values (caar alist)
			       (cdar alist)))
		     cdr
		     (reverse alist)
		     comparator))

;; Predicates

(define (hash-table-contains? hash-table key)
  (and (hash-table-ref/default hash-table key #f) #t))

(define (hash-table-empty? hash-table)
  (zero? (hash-table-size hash-table)))

(define (hash-table=? value-comparator hash-table1 hash-table2)
  (call-with-current-continuation
   (lambda (return)
     (hash-table-for-each (lambda (key value)
			    (unless (=? value-comparator
					value
					(hash-table-ref hash-table2
							key
							(lambda ()
							  (return #f))))
			      (return #f)))
			  hash-table1)
     (hash-table-for-each (lambda (key value)
			    (unless (=? value-comparator
					value
					(hash-table-ref hash-table1
							key
							(lambda ()
							  (return #f))))
			      (return #f)))
			  hash-table2)
     #t)))

(define (hash-table-mutable? hash-table)
  #t)

;; Accessors

(define hash-table-ref
  (case-lambda
   ((hash-table key)
    (hash-table-ref hash-table
		    key
		    (lambda ()
		      (error "hash-table-ref: key not found" key))))
   ((hash-table key failure)
    (hash-table-ref hash-table key failure values))
   ((hash-table key failure success)
    (let ((entry (hash-table-entry hash-table
				   (hash-table-address hash-table key))))
      (if entry
	  (success (entry-value entry))
	  (failure))))))

(define (hash-table-ref/default hash-table key default)
  (hash-table-ref hash-table key (lambda () default)))

;;; Mutators

(define (hash-table-set! hash-table . args)
  (do ((args args (cddr args)))
      ((null? args))
    (%hash-table-set! hash-table (car args) (cadr args))))

(define (%hash-table-set! hash-table key value)
  (let* ((address (hash-table-address hash-table key))
	 (entry (hash-table-entry hash-table address)))
    (cond
     (entry
      (entry-set-value! entry value))
     (else
      (let ((entry (make-entry key value)))
	(hash-table-set-entry! hash-table address entry)
	(hash-table-add-entry! hash-table entry))))))

(define (hash-table-delete! hash-table . keys)
  (let loop ((count 0) (keys keys))
    (cond
     ((null? keys)
      count)
     (else
      (loop (+ count
	       (%hash-table-delete! hash-table (car keys)))
	    (cdr keys))))))

(define (%hash-table-delete! hash-table key)
  (let ((capacity (hash-table-capacity hash-table))
	(address (hash-table-address hash-table key)))
    (let ((entry (hash-table-entry hash-table address)))
      (cond
       ((not (hash-table-entry hash-table address))
	0)
       (else
	(hash-table-remove-entry! hash-table entry)
	(let loop1 ((i address))
	  (hash-table-set-entry! hash-table i #f)
	  (let loop2 ((j i))
	    (let ((j (modulo (+ 1 j) capacity)))
	      (let ((entry (hash-table-entry hash-table j)))
		(cond
		 ((not entry)
		  1)
		 (else
		  (let ((k (hash-table-index hash-table (entry-key entry))))
		    (cond
		     ((if (<= i j)
			  (and (< i k) (<= k j))
			  (or (< i k) (<= k j)))
		      (loop2 j))
		     (else
		      (hash-table-set-entry! hash-table i entry)
		      (loop1 j)))))))))))))))

(define (hash-table-intern! hash-table key failure)
  (let ((address (hash-table-address hash-table key)))
    (let ((entry (hash-table-entry hash-table address)))
      (cond
       (entry
	(entry-value entry))
       (else
	(let ((value (failure)))
	  (let ((entry (make-entry key value)))
	    (hash-table-set-entry! hash-table
				   address
				   entry)
	    (hash-table-add-entry! hash-table entry))
	  value))))))

(define hash-table-update!
  (case-lambda
   ((hash-table key updater)
    (hash-table-update! hash-table key updater (lambda ()
						 (error "hash-table-update: key not found" key))))
   ((hash-table key updater failure)
    (hash-table-update! hash-table key updater failure values))
   ((hash-table key updater failure success)
    (let ((entry (hash-table-entry hash-table (hash-table-address hash-table key))))
      (cond
       (entry
	(entry-set-value! (updater (success (entry-value entry)))))
       (else
	(failure)))))))

(define (hash-table-update!/default hash-table key updater default)
  (hash-table-update! hash-table key updater (lambda () default)))

(define (hash-table-pop! hash-table)
  (let ((entry (hash-table-head hash-table)))
    (%hash-table-delete! hash-table (entry-key entry))
    (values (entry-key entry) (entry-value entry))))

(define (hash-table-clear! hash-table)
  (hash-table-set-buckets! hash-table (make-vector 256 #f))
  (hash-table-set-size! hash-table 0)
  (hash-table-set-head! hash-table #f))

;;; The whole hash table

(define (hash-table-keys hash-table)
  (hash-table-fold (lambda (key value keys)
		     (cons key keys))
		   '()
		   hash-table))

(define (hash-table-values hash-table)
  (hash-table-fold (lambda (key value values)
		     (cons value values))
		   '()
		   hash-table))

(define (hash-table-entries hash-table)
  (values (hash-table-keys hash-table)
	  (hash-table-entries hash-table)))

(define (hash-table-find proc hash-table failure)
  (call-with-current-continuation
   (lambda (return)
     (hash-table-for-each (lambda (key value)
			   (let ((result (proc key value)))
			     (when result
			       (return result))))
			 hash-table)
     (failure))))

(define (hash-table-count pred hash-table)
  (hash-table-fold (lambda (key value count)
		     (if (pred key value)
			 (+ 1 count)
			 count))
		   0
		   hash-table))

;;; Mapping and folding

(define (hash-table-map proc comparator hash-table)
  (let ((new-hash-table (make-hash-table comparator)))
    (hash-table-for-each (lambda (key value)
			   (%hash-table-set! new-hash-table
					     key
					     (proc value)))
			 hash-table)
    new-hash-table))

(define (hash-table-for-each proc hash-table)
  (do ((head (hash-table-head hash-table) (entry-next head)))
      ((not head))
    (proc (entry-key head) (entry-value head))))

(define (hash-table-map! proc hash-table)
  (do ((head (hash-table-head hash-table) (entry-next head)))
      ((not head))
    (entry-set-value! head
		      (proc (entry-key head) (entry-value head)))))

(define (hash-table-map->list proc hash-table)
  (hash-table-fold (lambda (key value list)
		     (cons (proc key value) list))
		   '()
		   hash-table))

(define (hash-table-fold proc seed hash-table)
  (let loop ((seed seed) (head (hash-table-head hash-table)))
    (cond
     ((not head)
      seed)
     (else
      (loop (proc (entry-key head) (entry-value head) seed)
	    (entry-next head))))))

(define (hash-table-prune! proc hash-table)
  (hash-table-for-each (lambda (key value)
			 (when (proc key value)
			   (%hash-table-delete! hash-table key)))
		       hash-table))

;;; Copying and conversion

(define hash-table-copy
  (case-lambda
   ((hash-table mutable?)
    (hash-table-copy hash-table))
   ((hash-table)
    (hash-table-map values (hash-table-comparator hash-table) hash-table))))

(define (hash-table-empty-copy hash-table)
  (make-hash-table (hash-table-comparator hash-table)))

(define (hash-table->alist hash-table)
  (hash-table-fold (lambda (key value alist)
		     (cons (cons key value) alist))
		   '()
		   hash-table))

;;; Hash tables as sets

(define (hash-table-union! hash-table1 hash-table2)
  (hash-table-for-each (lambda (key value)
			 (hash-table-intern! hash-table1 key (lambda () value)))
		       hash-table2)
  hash-table1)

(define (hash-table-intersection! hash-table1 hash-table2)
  (hash-table-prune! (lambda (key value)
		       (not (hash-table-contains? hash-table2 key)))
		     hash-table2)
  hash-table1)

(define (hash-table-difference! hash-table1 hash-table2)
  (hash-table-prune! (lambda (key value)
		       (hash-table-contains? hash-table2 key))
		     hash-table2)
  hash-table1)

(define (hash-table-xor! hash-table1 hash-table2)
  (hash-table-difference! hash-table1 hash-table2)
  (hash-table-union! hash-table1 hash-table2))

;;; Utility procedures

(define-record-type <entry>
  (make-entry key value)
  entry?
  (key entry-key)
  (value entry-value entry-set-value!)
  (prev entry-prev entry-set-prev!)
  (next entry-next entry-set-next!))

(define (hash-table-capacity hash-table)
  (vector-length (hash-table-buckets hash-table)))

(define (hash-table-index hash-table key)
  (let ((hash (comparator-hash (hash-table-comparator hash-table) key)))
    (modulo (+ (* 1103515245 hash) 12345)
	    (hash-table-capacity hash-table))))

(define (hash-table-address hash-table key)
  (let ((capacity (hash-table-capacity hash-table)))
    (let loop ((index (hash-table-index hash-table key)))
      (let ((entry (hash-table-entry hash-table index)))
	(cond
	 ((not entry)
	  index)
	 ((=? (hash-table-comparator hash-table)
	      key
	      (entry-key entry))
	  index)
	 (else
	  (loop (modulo (+ 1 index) capacity))))))))

(define (hash-table-entry hash-table index)
  (vector-ref (hash-table-buckets hash-table) index))

(define (hash-table-set-entry! hash-table index entry)
  (vector-set! (hash-table-buckets hash-table) index entry))

(define (hash-table-add-entry! hash-table entry)
  (entry-set-prev! entry #f)
  (entry-set-next! entry (hash-table-head hash-table))
  (hash-table-set-head! hash-table entry)
  (when (entry-next entry)
    (entry-set-prev! (entry-next entry) entry))
  (let ((size (+ (hash-table-size hash-table) 1)))
    (hash-table-set-size! hash-table size)
    (when (>= size (quotient (hash-table-capacity hash-table) 2))
      (hash-table-rebuild! hash-table))))

(define (hash-table-remove-entry! hash-table entry)
  (cond
   ((not (entry-prev entry))
    (hash-table-set-head! hash-table (entry-next entry)))
   (else
    (entry-set-next! (entry-prev entry) (entry-next entry))))
  (let ((size (- (hash-table-size hash-table) 1)))
    (hash-table-set-size! hash-table size)))

(define (hash-table-rebuild! hash-table)
  (hash-table-set-buckets! hash-table
			   (make-vector (* 2 (hash-table-capacity hash-table))
					#f))
  (do ((head (hash-table-head hash-table) (entry-next head)))
      ((not head))
    (let ((address (hash-table-address hash-table (entry-key head))))
      (hash-table-set-entry! hash-table address head))))
