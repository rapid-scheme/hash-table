;; Copyright (C) 2018 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Hash tables compatible with SRFI 125

(define-library (rapid hash-table)
  (export make-hash-table
	  hash-table
	  hash-table-unfold
	  alist->hash-table

	  hash-table?
	  hash-table-contains?
	  hash-table-empty?
	  hash-table=?
	  hash-table-mutable?

	  hash-table-ref
	  hash-table-ref/default

	  hash-table-set!
	  hash-table-delete!
	  hash-table-intern!
	  hash-table-update!
	  hash-table-update!/default
	  hash-table-pop!
	  hash-table-clear!

	  hash-table-size
	  hash-table-keys
	  hash-table-values
	  hash-table-entries
	  hash-table-find
	  hash-table-count

	  hash-table-map
	  hash-table-for-each
	  hash-table-map!
	  hash-table-map->list
	  hash-table-fold
	  hash-table-prune!

	  hash-table-copy
	  hash-table-empty-copy
	  hash-table->alist

	  hash-table-union!
	  hash-table-intersection!
	  hash-table-difference!
	  hash-table-xor!)
  (import (scheme base)
	  (scheme case-lambda)
	  (rapid receive)
	  (rapid comparator))
  (include "hash-table.scm"))
